#! /bin/sh

# This can be used as a pre-commit script.  Just run
#   cp run_tests.sh .git/hooks/pre-commit
# and it will run before each commit.

set -xue

go generate ./...
go install -v ./cmd/*
go list ./... | xargs go test
find ./* -type f -name '*.go' -print0 | xargs -0 gofmt -d
go list ./... | xargs go vet
if ! type golint > /dev/null; then
  go get -u golang.org/x/lint/golint
fi
go list ./... | xargs -L1 golint -set_exit_status
