# GQ GMC

This is a web service interface for GQ GMC geiger counters.

## Installation

Note this is not yet complete.

```bash
go get -u gitlab.com/lyda/gqgmc/cmd/gqgmcd
```

## Configuring

Command line:

```
Usage of gqgmcd:
      --config string           Config file (default "gqgmc.conf")
      --device string           Device for Geiger Counter (default "/dev/gqgmc")
      --listen-address string   Address for HTTP requests (default ":8080")
      --model string            Model of Geiger Counter (default "gqgmc")
      --sleep-cycle int         Seconds to sleep per cycle. (default 5)
```

The config file uses the same variables (not `config` obviously)
but with dashes replaced with underscores. So `sleep-cycle = 10`
for instance.

## Prometheus variables

There is a sample `prometheus.yml` configuration in the `docs/prometheus/`
directory.  See the Prometheus
[getting started](https://prometheus.io/docs/introduction/getting_started/)
docs for more information.

* `gqgmc_geiger_cpm` (histogram) CPM readings.
* `gqgmc_geiger_cps` (histogram) CPS readings.
* `gqgmc_power_volts` (histogram) Voltage readings.
* `gqgmc_sys_errors` (gauge) Error counts. Records problems
  communicating with the device.

There are also sample consoles in the `docs/prometheus/` directory. Add this
to the left hand side menu in `menu.lib` your prometheus install:

```
{{ if query "up{job='gqgmc'}" }}
{{ template "_menuItem" (args . "gqgmc.html" "GQGMC") }}
{{ if match "^gqgmc" .Path }}
  {{ if .Params.instance }}
  <ul>
    <li {{ if eq .Path "gqgmc-overview.html" }}class="prom_lhs_menu_selected"{{ end }}>
      <a href="gqgmc-overview.html?instance={{ .Params.instance }}">{{.Params.instance }}</a>
    </li>
  </ul>
  {{ end }}
{{ end }}
{{ end }}
```

![Prometheus Console](/docs/gqgmc.png)

## Configuring udev

Copy the `linux/51-gqgmc.rules` file to the `/etc/udev/rules.d`
directory and force the reload of udev rules:

```bash
sudo cp ./51-gqgmc.rules /etc/udev/rules.d/51-gqgmc.rules sudo
udevadm control --reload-rules
```

Disconnect the GQ GMC-300 from the computer and then reconnect.
Verify that there exists a `/dev/gqgmc` in the `/dev` directory
with read/write permission for all users.

```bash
ls -la /dev/gqgmc
```

## Development Resources

There's a GQ Electronics [forum](https://www.gqelectronicsllc.com/forum/) which
isn't hugely active, but some really good info and helpful people
there. If you follow the git log on this project you'll see this
is based off a C++ implementation which is
[over on sourceforge](https://sourceforge.net/projects/gqgmc/)

## TODO

There's still a fair bit to do. This works good enough for my purposes so
I'm not likely to do much more. I've seeded the
[issue tracker](https://gitlab.com/lyda/gqgmc/issues) with the obvious
issues.
[Merge Requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
welcome!
