//
// geiger.go
// Copyright (C) 2017 kevin <kevin@ie.suberic.net>
//
// Distributed under terms of the GPL license.
//

package geiger

import "time"

// New creates a new Counter instance
func New(c Config) (Counter, error) {
	switch c.Model {
	case "gqgmc":
		return NewGQGMC(c)
	}
	return nil, nil
}

// Counter is an interface for Geiger Counters
type Counter interface {
	Clear() error
	Model() string
	Version() string
	Serial() string
	GetCPM() (uint16, error)
	GetCPS() (uint16, error)
	Volts() (int16, error)
	GetHistory()
	TurnOnCPS() error
	TurnOffCPS() error
	GetAutoCPS() (uint16, error)
	TurnOffPower()
	GetConfiguration() (*DevConfig, error)
	SetConfiguration()
	SetTime(time.Time)
	GetTime() (time.Time, error)
	GetTemp() (float64, error)
	GetGyro() (int16, int16, int16, error)
	FactoryReset()
	Reboot()
}

// Config contain the configuration for a Geiger Counter
type Config struct {
	Model   string
	Device  string
	Options map[string]string
}

// Reading contains a single geiger reading
type Reading struct {
	CPM float64
}
