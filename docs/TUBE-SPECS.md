# Product Description 

Geiger Muller counter/GM tube M4011

Geiger Muller tube J302, for detail please contact us.

## Model M4011 countertube

The detector is oxide cathode tube, coaxial cylindrical structure
of thin walled (the denity of tube wall50±10cg/cm2), the Pulse
application-oriented halogen tube, The ambient temperature from
-40°C~55°C range that is exploring 20mR/h-120mR/hγ-ray and 100~1800
off variables/min. cm 2 softβ-ray.

- Diameter: Φ10±0.5mm
- Length: 90±2mm
- Starting voltage:<350V
- Minimum plateau rang:80v
- Maximum plateau slope:10%/80v
- Recommended working voltage: 380V
- Maximum count rate:25times/min
- Limit working voltage: 550V
- Life expectancy: >1×109pulse

### Keep shell clean

The countetube should keep a high clean. To minimize surface
contamination, otherwise it will reduce the insulation resistance.

### Apply the typical working conditions

The characteristic parameters of counter are measured under the
recommended operating voltage. so the user should be possible to
use the recommended operationg voltage to obtain the given parameters.

### Installation with caution

When you installation the counter should pay attention to it, as
counter output leads as short as possible. Andode and cathode leads
to extremely excessive force shall not be. Especially the tube of
glass shell should be careful, otherwise it will cause the pipe
leak or shell broken.
