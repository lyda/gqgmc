//
// main.go
// Copyright (C) 2017 kevin <kevin@ie.suberic.net>
//
// Distributed under terms of the MIT license.
//

package main

import (
	flag "github.com/spf13/pflag"
	"log"
	"net/http"

	"gitlab.com/lyda/gqgmc/config"
	"gitlab.com/lyda/gqgmc/devices/geiger"
	"gitlab.com/lyda/gqgmc/server/metrics"
	"gitlab.com/lyda/gqgmc/server/pages"
)

var (
	addr       = flag.String("listen-address", ":8080", "Address for HTTP requests")
	device     = flag.String("device", "/dev/gqgmc", "Device for Geiger Counter")
	model      = flag.String("model", "gqgmc", "Model of Geiger Counter")
	sleepCycle = flag.Int64("sleep-cycle", 5, "Seconds to sleep per cycle.")
	cfg        = flag.String("config", "", "Config file")
)

func main() {
	flag.Parse()
	c, err := config.ReadConfig(*cfg)
	if err != nil {
		log.Printf("Couldn't read config: %s\n", err)
		return
	}

	gc, gcErr := geiger.New(geiger.Config{Model: c.Model, Device: c.Device})
	if gcErr != nil {
		log.Printf("Error: %s\n", gcErr)
	}

	p := pages.New(gc, gcErr)
	p.Register()

	if gcErr == nil {
		m := metrics.Register(gc)
		go m.Gather(c.SleepCycle)
	}
	log.Fatal(http.ListenAndServe(c.ListenAddress, nil))
}
