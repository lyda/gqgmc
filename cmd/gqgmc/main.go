//
// main.go
// Copyright (C) 2017 kevin <kevin@phrye.com>
//
// Distributed under terms of the GPL license.
//

package main

import (
	"fmt"
	"time"

	"gitlab.com/lyda/gqgmc/devices/geiger"
)

func main() {
	var (
		gc       geiger.Counter
		cfg      *geiger.DevConfig
		cps, cpm uint16
		volts    int16
		temp     float64
		err      error
		t        time.Time
	)

	gc, err = geiger.New(geiger.Config{
		Model:  "gqgmc",
		Device: "/dev/gqgmc",
	})
	if err != nil {
		fmt.Printf("Failed to connect to geiger counter: '%s'\n", err)
		return
	}

	t, err = gc.GetTime()
	if err != nil {
		fmt.Printf("Failed: '%s'\n", err)
	} else {
		fmt.Printf("Time: %s\n", t)
	}

	cpm, err = gc.GetCPM()
	if err != nil {
		fmt.Printf("CPM failed: '%s'\n", err)
	} else {
		fmt.Printf("CPM: %d\n", cpm)
	}

	cps, err = gc.GetCPS()
	if err != nil {
		fmt.Printf("CPS failed: '%s'\n", err)
	} else {
		fmt.Printf("CPS: %d\n", cps)
	}

	fmt.Printf("Version: %s\n", gc.Version())
	fmt.Printf("Model: %s\n", gc.Model())
	fmt.Printf("Serial: %s\n", gc.Serial())

	volts, err = gc.Volts()
	if err != nil {
		fmt.Printf("Volts failed: '%s'\n", err)
	} else {
		fmt.Printf("Volts: %d\n", volts)
	}

	temp, err = gc.GetTemp()
	if err != nil {
		fmt.Printf("Temp failed: '%s'\n", err)
	} else {
		fmt.Printf("Temp: %g\n", temp)
	}

	cfg, err = gc.GetConfiguration()
	if err != nil {
		fmt.Printf("Failed to get config: '%s'\n", err)
	} else {
		fmt.Printf("Cfg: %+v\n", cfg)
	}

}
