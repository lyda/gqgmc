//
// config.go
// Copyright (C) 2017 kevin <kevin@phrye.com>
//
// Distributed under terms of the GPL license.
//

package config

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Config for storing configuration.
type Config struct {
	ListenAddress string `mapstructure:"listen_address"`
	Device        string `mapstructure:"device"`
	Model         string `mapstructure:"model"`
	SleepCycle    int64  `mapstructure:"sleep_cycle"`
}

func setDefaults() {
	viper.BindPFlag("listen_address", pflag.Lookup("listen-address"))
	viper.BindPFlag("device", pflag.Lookup("device"))
	viper.BindPFlag("model", pflag.Lookup("model"))
	viper.BindPFlag("sleep_cycle", pflag.Lookup("sleep-cycle"))
}

// ReadConfig reads the client configuration from a file into a Config struct.
func ReadConfig(cfg string) (*Config, error) {
	setDefaults()
	if cfg != "" {
		viper.SetConfigFile(cfg)
		viper.SetConfigType("hcl")
		if err := viper.ReadInConfig(); err != nil {
			return nil, err
		}
	}
	c := &Config{}
	if err := viper.Unmarshal(c); err != nil {
		return nil, err
	}
	return c, nil
}
